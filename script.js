/**
 * Created on 11.07.2019.
 */
$(document).ready(function () {
    $('.header-menu li a').on('click',function () {
        $('html, body').animate({
            scrollTop:$($(this).attr('href')).offset().top
        },700);
    });

    let top100vh=$('.border').offset().top;
    $(window).scroll(function () {
       $('.btn-to-top')['fade'+ ($(this).scrollTop() >= top100vh ? 'In': 'Out')](500)
   });

    $('.btn-to-top').on('click', function () {
        $('html, body').animate({
            scrollTop:$($(this).attr('href')).offset().top+'px'
        },700);
    });

    $('.hide-news').on('click',function () {
         $('.container.hot-news').slideToggle('slow');
    })

});